#include<iostream>
#include<algorithm>

using namespace std;

void printArr(int arr[], int len){
    for(int i = 0; i < len; ++i){
        cout << arr[i] << "";
    }
    cout << endl;
}

bool isValidInput(int input){
    int validInput[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

    for(int i = 0; i < 9; ++i){
        if(validInput[i] == input) return true;
    }
    return false;
}

int* fillMissing(int arr[], int len){
        sort(arr, arr + len);
        int *tmpArr = new int[9];
        for(int i = 0; i < len; ++i){
            tmpArr[arr[i] - 1] = arr[i];
        }
        for(int i = 0; i < 9; ++i){
            if(!tmpArr[i]){
                tmpArr[i] = i + 1;
            }
        }
        return tmpArr;
       
}

int main(){
    int len = 9;
    int *arr = new int[len];
    int userInput = 1;
    int i = 0;

    while(userInput){
        cout << "Unesite broj od 1 do 9, nula je za prekid:" << endl;
        cin >> userInput;
        if(userInput == 0) break;
        if(!isValidInput(userInput)){
            cout << "Krivi unos, pokusajte ponovo" << endl;
            continue;
        }
        arr[i++] = userInput;
    }

    arr = fillMissing(arr, i);
    printArr(arr, len);
    delete[] arr; 
    
}