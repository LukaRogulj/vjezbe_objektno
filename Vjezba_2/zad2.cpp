#include<iostream>

using namespace std;


void swapInt(int& a, int &b){
    int tmp = a;
    a = b;
    b = tmp;
}

int* sortEvenOdd(int arr[], int len){

    int l = 0, r = len - 1;
    int k = 0;

    while( l < r ){
        //Find firs even number from the right
        while(arr[l]%2 == 0){
            l++;
        }
        //find first odd number from the left
        while(arr[r]%2 != 0){
            r--;
        }
        if(l < r){
            swapInt(arr[l], arr[r]);
        }
    }
    return arr;
}

void printArr(int arr[], int len){
    for(int i = 0; i < len; i++){
        cout << arr[i] << " ";
    }
    cout << endl;
}

int main(){

    int len = 10;
    int *arr = new int[len];

    for(int i = 0; i < len; ++i){
        cout << "Unesite broj:" << endl;
        cin >> arr[i];
    }

    arr = sortEvenOdd(arr, len);
    printArr(arr, len);
    delete[] arr;
    

}