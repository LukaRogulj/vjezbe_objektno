#pragma comment(lib, "Ws2_32.lib")
#include <iostream>


template <typename T>
class Stack {
	const int defaultSize = 10;
	const int maxSize = 1000;
	int capacity;
	int top;
	T* stackPtr;

public:
	Stack() {
		stackPtr = new T[defaultSize];
		capacity = defaultSize;
		top = -1;
	}
	Stack(int size) {
		if (size < defaultSize || size > maxSize) size = defaultSize;
		stackPtr = new T[size];
		capacity = size;
		top = -1;
	}
	~Stack() { delete[] stackPtr; };

	void push(T n) {
		if (isFull()) {
			std::cout << "Overflow" << std::endl;
			exit(EXIT_FAILURE);
		}
		stackPtr[++top] = n;
	}
	T pop() {
		if (isEmpty()) {
			std::cout << "Underflow" << std::endl;
			exit(EXIT_FAILURE);
		}
		return stackPtr[top--];
	}
	T peek() {
		if (isEmpty()) {
			exit(EXIT_FAILURE);
		}
		return stackPtr[top];
	}
	int cap() const {
		return capacity;
	}
	int size() const {
		return top + 1;
	}
	bool isEmpty() const {
		return top == -1;
	}
	bool isFull() const {
		return size() == capacity;
	}

};

