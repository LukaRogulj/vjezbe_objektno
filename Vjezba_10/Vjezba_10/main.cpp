#include<iostream>
#include<string>
#include<exception>
#include<fstream>

using namespace std;

class OperatorException :
	public exception {

	virtual const char* what() const throw() {
		return "Krivi operand unesen";
	}
};

class InputException :
	public exception {

	virtual const char* what() const throw() {
		return "Nije unesen integer";
	}
};

class DivisionException :
	public exception {

	virtual const char* what() const throw() {
		return "Djeljenje s nulom";
	}
};

class Math {
public:
	int inputNumber() {
		int num;
		if (!(cin >> num)) {
			throw InputException();
		}
		return num;
	}

	char inputOperator() {
		char oper;
		cout << "Unesite operand (*, /, +, -)" << endl;
		cin >> oper;
		if (oper != '+' && oper != '-' && oper != '*' && oper != '/') {
			throw OperatorException();
		}
		return oper;
	}

	void applyOperation(int num1, int num2, char oper) {
		if (oper == '*') {
			cout << num1 * num2 << endl;
		}
		else if (oper == '/') {
			if (num2 == 0) {
				throw DivisionException();
			}
			cout << num1 / num2 << endl;
		}
		else if (oper == '+') {
			cout << num1 + num2 << endl;
		}
		else if (oper == '-') {
			cout << num1 - num2 << endl;
		}
	}
};



class Logg {

public:
	void logError(string error) {
		ofstream fout("errors.log", ios_base::out | ios_base::app);
		fout << error << endl;
		cout << error << endl;
		fout.close();
	}


};

int main() {

	Logg log;
	Math m;

	while (true) {
		try {
			int a = m.inputNumber();
			int b = m.inputNumber();
			char o = m.inputOperator();
			m.applyOperation(a, b, o);

		}
		catch (exception & e) {
	
			log.logError(e.what());
			return 0;
		}

	}

	return 0;
}
