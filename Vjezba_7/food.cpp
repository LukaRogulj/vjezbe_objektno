#include<iostream>
#include"food.h"
#include<ctime>
#include<algorithm>

using namespace std;

Food::Food(const Food &f2){
    type = f2.type;
    name = f2.name;
    waterContent =f2.waterContent;
    exprDate = f2.exprDate;
    proteinAmount = f2.proteinAmount;
    fatAmount = f2.fatAmount;
    carbAmount = f2.carbAmount;
    dailyUsage = f2.dailyUsage;

    int size = sizeof(f2.consumption)/sizeof(UsedFood);
    consumption =  new UsedFood[allocateConsump(exprDate)];
    copy(f2.consumption,f2.consumption + size, consumption);

}


Food::Food(string typeA, string nameA, float waterContentA, float proteinAmountA,
         float fatAmountA, float carbAmountA, float dailyUsageA, string exprDateA){

    type = typeA;
    name = nameA;
    waterContent = waterContentA;    
    exprDate = exprDateA;
    proteinAmount = proteinAmountA;
    fatAmount = fatAmountA;
    carbAmount = carbAmountA;
    dailyUsage = dailyUsageA;

    consumption = new UsedFood[allocateConsump(exprDate)];
}

int Food::allocateConsump(string date) const{
    int year = stoi(date.substr(date.length() - 4));

    time_t now = time(0);
    tm *ltm = localtime(&now);
    int currYear = 1970 + ltm->tm_year;

    int toAllocate = currYear + year;

    if(toAllocate == 0){
        return 12;
    }

    return toAllocate * 12;

}

string Food::getType() const{
    return type;
}
string Food::getName() const{
    return name;
}
float Food::getWaterContent() const{
    return waterContent;
}
float Food::getProteinAmount() const{
    return proteinAmount;
}
float Food:: getFatAmount() const{
    return fatAmount;
}
float Food::getCarbAmount() const{
    return carbAmount;
}
int Food::getDailyUsage() const{
    return dailyUsage;
}
string Food::getExprDate() const{
    return exprDate;
}

float UsedFood::getKg() const{
    return kg;
}
int UsedFood::getMonth() const{
    return month;
}
int UsedFood::getYear() const{
    return year;
}

void Food::incDailyUsage(){
    dailyUsage++;
}
void Food::decDailyUsage(){
    dailyUsage--;
}

bool Food::isCurrYear(int year){
    

    time_t now = time(0);
    tm *ltm = localtime(&now);
    int currYear = 1970 + ltm->tm_year;

    return currYear == year;
}
int Food::getCurrentIndex() const{
    return currentIndex;
}
void Food::incCurrentIndex(){
    currentIndex++;
}

void Food::addConsuption(UsedFood food){

    if(!(isCurrYear(food.getYear()))){
        return;
    }

    for(int i = 0; i < currentIndex; i++){
        if(!(food.getMonth() == consumption[i].getMonth())){
            return;
        }
    }
    consumption[currentIndex] = food;
    incCurrentIndex();

}

float Food::checkConsumption(){
    
    float sum = 0;
    int i = currentIndex;
    int counter = 0;
    
    while(i){
        sum += consumption[i].getKg();
        --i;
        ++counter;
        if(counter == 11) break;
    }
    /* for(int i = 0; i < currentIndex; i++){
        sum += consumption[i].getKg();
    } */
    sum = sum / (currentIndex * 30);
    
    return dailyUsage - ((sum * 0.1) + sum);
}

void Food::printObj(){
    cout << type << endl;
    cout << name << endl;
    cout << waterContent << endl;
    cout << exprDate << endl;
    cout << proteinAmount << endl;
    cout << fatAmount << endl;
    cout << carbAmount << endl;
    cout << dailyUsage << endl;
    for(int i = 0; i < currentIndex; i++){
        cout << consumption[i].getKg() << endl;
        cout << consumption[i].getMonth() << endl;
        cout << consumption[i].getYear() << endl;
    }
}

UsedFood::UsedFood(int year, int month, float kg){
    this->year = year;
    this->month = month;
    this->kg = kg;
}

UsedFood::UsedFood(){}

Food::~Food(){
    delete []consumption;
}
