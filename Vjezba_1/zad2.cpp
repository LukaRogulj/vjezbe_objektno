#include<iostream>
#include<iomanip>

using namespace std;

typedef struct{

    int id;
    char *ime;
    char spol;
    int ocjenaKviz1;
    int ocjenaKviz2;
    int ocjenaSredinaSemestra;
    int ocjenaKrajSemestra;
    int ukupnoBodova;

}Student;


void addStudent(Student s[], int index){


    cout << "Unesite ime studenta: " << endl;
    cin >> s[index].ime;
    cout << "Unesite spol studenta M/Z: " << endl;
    cin >> s[index].spol;
    cout << "Unesite ocjenu prvog kviza: " << endl;
    cin >> s[index].ocjenaKviz1;
    cout << "Unesite ocjenu drugog kviza: " << endl;
    cin >> s[index].ocjenaKviz2;
    cout << "Unesite ocjenu na sredini semestra: " << endl;
    cin >> s[index].ocjenaSredinaSemestra;
    cout << "Unesite ocjenu na kraju semestra: " << endl;
    cin >> s[index].ocjenaKrajSemestra;

    s[index].ukupnoBodova = s[index].ocjenaKviz1 + s[index].ocjenaKviz2 + s[index].ocjenaSredinaSemestra + s[index].ocjenaKrajSemestra;

    s[index].id = index;

}

void printStudent(Student s[], int i){

    if(s[i].ime == NULL){       // provjera dali student postoji
        return;
    }

    cout << "\n\nId studenta: " << endl;
    cout << s[i].id << endl;
    cout << "Ime studenta: " << endl;
    cout << s[i].ime << endl;
    cout << "Spol studenta: " << endl;
    cout << s[i].spol << endl;
    cout << "Ocjena prvog kviza: " << endl;
    cout << s[i].ocjenaKviz1 << endl;
    cout << "Ocjena drugog kviza: " << endl;
    cout << s[i].ocjenaKviz2 << endl;
    cout << "Ocjena na sredini semestra: " << endl;
    cout << s[i].ocjenaSredinaSemestra << endl;
    cout << "Ocjena na kraju semestra: " << endl;
    cout << s[i].ocjenaKrajSemestra << endl;
    cout << "Ukupan broj bodova: " << endl;
    cout << s[i].ukupnoBodova << endl;
    cout << "\n\n" << endl;


}

 void printStudents(Student s[], int len){
    
    for(int i = 0; i < len; i++){
        printStudent(s, i); 
    }
}

 void findId(Student s[], int id){

    for(int i = 0; i < 20; i++){
        if(s[i].id == id){
            printStudent(s, i);
            return;
        }
    }
    std::cout << "\n\nNe postoji student s tim ID-om\n\n" << std::endl;
    return;
} 

void deleteStudent(Student& s){


    s.ime = NULL;
    s.spol = 0;
    s.ocjenaKviz1 = 0;
    s.ocjenaKviz2 = 0;
    s.ocjenaKrajSemestra = 0;
    s.ukupnoBodova = 0;
}

void updateStudent(Student& s){

    cout << "Unesite ime studenta: " << endl;
    cin >> s.ime;
    cout << "Unesite spol studenta M/Z: " << endl;
    cin >> s.spol;
    cout << "Unesite ocjenu prvog kviza: " << endl;
    cin >> s.ocjenaKviz1;
    cout << "Unesite ocjenu drugog kviza: " << endl;
    cin >> s.ocjenaKviz2;
    cout << "Unesite ocjenu na sredini semestra: " << endl;
    cin >> s.ocjenaSredinaSemestra;
    cout << "Unesite ocjenu na kraju semestra: " << endl;
    cin >> s.ocjenaKrajSemestra;

    s.ukupnoBodova = s.ocjenaKviz1 + s.ocjenaKviz2 + s.ocjenaSredinaSemestra + s.ocjenaKrajSemestra;
    
}

void avrgGrade(Student s){
    float avrg;
    avrg = static_cast<float>(s.ocjenaKviz1 + s.ocjenaKviz2 + s.ocjenaSredinaSemestra + s.ocjenaKrajSemestra) / 4;
    cout.precision(2);
    cout << avrg << endl;
}

void maxGrade(Student s[], int len){

    int tmp = 0;
    for(int i = 0; i < len - 1; ++i){
        if(s[i].ukupnoBodova < s[i+1].ukupnoBodova){
            tmp = s[i + 1].id;
        }
    }
    cout << "Student s najvecom ukupnom ocjenom:" << endl;
    printStudent(s, tmp);
}

void minGrade(Student s[], int len){

    int tmp = 0;
    for(int i = 0; i < len - 1; ++i){
        if(s[i].ime == NULL){
            continue;
        }

        if(s[i].ukupnoBodova < s[i+1].ukupnoBodova){
            tmp = s[i].id;
        }
    }
    cout << "Student s najmanjom ukupnom ocjenom:" << endl;
    printStudent(s, tmp);

}

Student* sortByGrade(Student s[], int len){

    Student tmp;

    for(int i = 0; i < len; ++i){
        for(int j = 0; j < len - i - 1; ++j){
            if(s[j].ukupnoBodova > s[j+1].ukupnoBodova){
                tmp = s[j];
                s[j] = s[j+1];
                s[j+1] = tmp;
            }
        }
    }
    return s;
}


int main(){

Student nizStudenta[128];
Student *sortStudent;
int unos = 0;
int idStudenta = 0;
int len = 0;

 while(1){
     cout << "Odaberite radnju" << endl;
     cout << "1. Dodaj studenta" << endl;
     cout << "2. Ukloni studenta " << endl;
     cout << "3. Ažuriraj studenta" << endl;
     cout << "4. Prikazi sve studente" << endl;
     cout << "5. Prosjek bodova za studenta" << endl;
     cout << "6. Student s najvecim brojem bodova" << endl;
     cout << "7. Student s najmanjim brojem bodova" << endl;
     cout << "8. Pronadi studenta po ID" << endl;
     cout << "9. Sortiraj studente po broju bodova" << endl;
     cout << "10. Izlaz" << endl;


     cin >> unos;
     if( unos != 1 && len == 0 ){
         cout << "\nPrvo unesite studenta\n" << endl;
         continue;
     }

     switch (unos)
     {
         case 1:
            addStudent(nizStudenta, len);
            ++len;
            break;
        case 2:
            cout << "Studenta s kojim ID zelite izbrisati" << endl;
            cin >> idStudenta;
            deleteStudent(nizStudenta[idStudenta]);
            break;
        case 3:
            cout << "Unesite ID studenta kojega zelite azurirati" << endl;
            cin >> idStudenta;
            if(idStudenta > len){
                cout << "\n\nUnesite ID postojeceg studenta!!!\n\n" << endl;
                continue;
            }
            updateStudent(nizStudenta[idStudenta]);
            break;
        case 4:
            printStudents(nizStudenta, len);
            break;
        case 5:
            cout << "Unesite ID studenta ciji zelite prosjek bodova" << endl;
            cin >> idStudenta;
            avrgGrade(nizStudenta[idStudenta]);
            break;
        case 6:
            maxGrade(nizStudenta, len);
            break;
        case 7:
            minGrade(nizStudenta, len);
            break;
        case 8:
            cout << "Unesite ID trazenog studenta" << endl;
            cin >> idStudenta;
            findId(nizStudenta, idStudenta);
            break;
        case 9:
            sortStudent = sortByGrade(nizStudenta, len);
            printStudents(sortStudent, len);
            break;
        case 10:
            exit(0);
     
         default:
            cout << "Krivi unos" << endl;
             break;
     }
 }

}
