#include<string>
#include<vector>

using namespace std;

class UsedFood{
    int year;
    int month;
    float kg;

    public:
    UsedFood(int year, int month, float kg);
    int getYear() const;
    int getMonth() const;
    float getKg() const;
    UsedFood();
};

class Food{
    string type;
    string name;
    float waterContent;
    string exprDate;
    float proteinAmount;
    float fatAmount;
    float carbAmount;
    float dailyUsage;
    UsedFood *consumption;
    int currentIndex = 0;
    
    public:
    Food(string type, string name, float waterContent, float proteinAmount, float fatAmount,
         float carbAmount, float dailyUsage, string exprDate);
    Food(const Food &f2);
    ~Food();
    string getType() const;
    string getName() const;
    float getWaterContent() const;
    float getProteinAmount() const;
    float getFatAmount() const;
    float getCarbAmount() const;
    int getDailyUsage() const;
    string getExprDate() const;
    void incDailyUsage();
    void decDailyUsage();
    bool isCurrYear(int year);
    void addConsuption(UsedFood);
    int getCurrentIndex() const;
    void incCurrentIndex();
    float checkConsumption();
    void printObj();


    int allocateConsump(string exprDate) const;

    /* Point(const Point &p2) {x = p2.x; y = p2.y; }  */
};

