#include<string>
#include<iostream>
#include<vector> 

using namespace std;

class VideoGames {

    public:
    virtual string type() = 0;
    virtual ~VideoGames(){};
    virtual vector<string> platforms() = 0;

    protected:
};

class Action: public VideoGames{

    protected:
    string type = "Action";

    public:
    Action();
    ~Action();
    virtual vector <string> platforms(){};
};

class Rpg: public VideoGames {

    protected:
    string type = "Rpg";

    public:
    Rpg(); 
    ~Rpg();
    virtual vector <string> platforms(){};
};

class OpenWorld: public VideoGames {

    protected:
    string type = "OpenWorld";
    
    public:
    OpenWorld();
    ~OpenWorld();
    virtual vector <string> platforms(){};
};

class LastOfUs2: public Action {

    public:
    string get_type();

    protected:
    vector <string> _platforms;
};

class GodOfWar: public Action {

    public:
    GodOfWar();
    string get_type();
    vector <string> platforms();
    void insert(string s);

    protected:
    vector<string> _platforms;
};

class DarkSouls: public Rpg {

    public:
    string get_type();
    
    protected:
    vector <string> _platforms;
};

class Fallout4: public Rpg {
    
    public:
    string get_type();

    protected:
    vector <string> _platforms;
};

class Witcher3: public OpenWorld, public Rpg {
    string type;
    public:

    Witcher3();
    string get_type();

    protected:
    string type;
    vector <string> _platforms;

};