#include"games.h"
#include<iostream>
#include<string>
#include<vector>

using namespace std;

Witcher3::Witcher3(){
    type = "OpenWorld Rpg";
}
string Witcher3::get_type() {
    return type;
}

string LastOfUs2::get_type(){
    return type;
}

string GodOfWar::get_type(){
    return type;
}

string DarkSouls::get_type(){
    return type;
}

string Fallout4::get_type(){
    return type;
}

vector<string> GodOfWar::platforms(){
    return _platforms;
}

void GodOfWar::insert(string s) {
    _platforms.push_back(s);
}