#include<iostream>
#include<vector>
#include"punivector.h"
#include<algorithm>

using namespace std;

vector<int> filterVector(vector <int> v1, vector <int> v2){

    sort(v1.begin(), v1.end());
    sort(v2.begin(), v2.end());

    vector <int> v3;
    for(int i = 0; i < v1.size(); ++i){
        if(!binary_search(v2.begin(), v2.end(), v1[i])){
            v3.push_back(v1[i]);
        }
    }
    return v3;
}