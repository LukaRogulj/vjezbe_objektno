#include<iostream>
#include<string>
#include<vector>
#include<cctype>
#include<iterator>

using namespace std;

bool isValidInput(string str){
    if(str.length() > 20){
        return false;
    }
    for(auto i : str){
        if(!((i >= '1' && i <= '9') || (i >= 'a' && i <='z') || (i >= 'A' && i <= 'Z'))){
            return false;
        }
    }
    return true;
}
void modifyString(string& str, bool choseType){
    int counter;
    char tmp;

    if(choseType){
        for(string::iterator it = str.begin(); it != str.end(); ++it){
            if(isdigit(*it) && isalpha(*(it + 1))){

                tmp = *(it + 1);
                counter = *(it) - '0';

                str.erase(it);

                for(int i = 0; i < counter - 1; ++i){
                    str.insert(it + i, tmp);
                }
            }
        }
    }

    if(!choseType){
        counter = 1;
        for(string::iterator it = str.begin(); it != str.end(); ++it){
            if(*it == *(it + 1)){
                tmp = *it;
                ++counter;
                continue;
            }
            if(counter > 1){
                for(int i = counter; i > 1; --i){
                    str.erase(it);
                    --it;
                }
                char a = char(counter + '0');
                str.insert(it, a);
                counter = 1;
            }
        }
    }
}

void printVector(vector <string> vec){
    for(int i = 0; i < vec.size(); ++i){
        cout << vec[i] << endl;
    }
}


int main(){
    int n;
    string buffer;
    cout << "Unesite N" << endl;
    cin >> n;
    vector <string> input;
    while(n){

        cout << "Unesite string" << endl;
        cin >> buffer;
        if(!(isValidInput(buffer))){
            cout << "Krivi unos" << endl;
            continue;
        }
        input.push_back(buffer);
        --n;
    }
    bool flag = false;
    for(int i = 0; i < input.size(); ++i){
        for(int j = 0; j < input[i].length(); ++j){
            if(isdigit(input[i][j])){
                flag = true;
                continue;
            }
        }
        modifyString(input[i], flag);
        flag = false;
    }
    printVector(input);
}

