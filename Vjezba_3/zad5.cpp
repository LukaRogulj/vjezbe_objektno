#include<iostream>
#include<vector>
#include<string>
#include<algorithm>

using namespace std;

typedef struct{
    string name;
    string movie;
    int year;
}Producent;

typedef struct{
    string name;
    int number;

}BrojFilmova;

void printNajzastupljenije(vector <BrojFilmova> a){
    for(int i = 0; i < a.size(); ++i){
        cout << a[i].name << endl;
        if(a[i].number > a[i + 1].number){
            break;
        }
    }
}

bool cmpFunction(const BrojFilmova& a, const BrojFilmova& b){
    return a.number > b.number;
}

void najzastupljenijiProducent(vector <BrojFilmova>& brFilmova){
      
    sort(brFilmova.begin(), brFilmova.end(), cmpFunction);  
    printNajzastupljenije(brFilmova);
}

void brojiFilmove(vector <Producent> producent){
    if(producent.empty()){
        return;
    }
     vector <BrojFilmova> brFilmova;
     BrojFilmova tmp;
 
    tmp.name = producent[0].name;
    tmp.number = 1;
    brFilmova.push_back(tmp);

    int count = producent.size();
    bool flag = false;
    for(int i = 1; i < count; ++i){
        for(int j = 0; j < brFilmova.size(); ++j){
            if(producent[i].name == brFilmova[j].name){
                brFilmova[j].number += 1;
                flag = false;
                break;
            }
            flag = true;
        }
        if(flag){
            tmp.name = producent[i].name;
            tmp.number = 1;
            brFilmova.push_back(tmp);
        }
    }  
    najzastupljenijiProducent(brFilmova);
}

int main(){

    vector <Producent> producent;

    for(int i = 0; i < 7; ++i)
    {
        producent.push_back(Producent());
        cout << "Unesite ime producenta:" << endl;
        cin >> producent[i].name;
        cout << "Unesite ime filma:" << endl;
        cin >> producent[i].movie;
        cout << "Unesite godinu filma:" << endl;
        cin >> producent[i].year;
    }

    brojiFilmove(producent);
    
    
}