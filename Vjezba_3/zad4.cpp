#include<iostream>
#include<vector>
#include<iterator>
#include<ctime>

using namespace std;

void playerOneTurn(vector <int>& matches);
void playerTwoTurn(vector <int>& matches);


int randomNumber(int min, int max, int len){

    if(len < max){max = len;}

    return min + (rand() % ( max - min + 1 ));
}

void removeMatches(int toRemove, vector <int>& matches){
    matches.resize(matches.size() - toRemove);
}

int checkStatus(vector <int> matches){
    return matches.size();
}

bool checkLose(vector <int> matches){
    if(!matches.size()){return true;}
}

void playerOneTurn(vector <int>& matches){

    static int playerOne;
    cout << "Unesite broj sibica:" << endl;
    cin >> playerOne;
    if(playerOne < 1 || playerOne > 4 || playerOne > matches.size()){
        cout << "Krivi unos!" << endl;
        playerOneTurn(matches);
    }
    removeMatches(playerOne, matches);
    if(checkLose(matches)){
        cout << "Player one lost" << endl;
        exit(1);
    }
    cout<< "Trenutni broj sibica: " << checkStatus(matches) << endl;
    playerTwoTurn(matches);
}

void playerTwoTurn(vector <int>& matches){

    static int playerTwo;
    cout << "Player two turn:" << endl;
    playerTwo = randomNumber(1, 4, matches.size());
    removeMatches(playerTwo, matches);
    if(checkLose(matches)){
        cout << "Player two lost" << endl;
        exit(1);
    }
    cout << "Trenutni broj sibica: "<< checkStatus(matches) << endl;
    playerOneTurn(matches);
}

int main(){

    srand(time(NULL));

    vector <int> matches(21, 1);
    playerOneTurn(matches);
}